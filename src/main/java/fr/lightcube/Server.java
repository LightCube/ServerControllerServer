package fr.lightcube;

import fr.lightcube.exception.NoJarFoundException;

import java.io.*;

/**
 * Created by Nitorac on 17/07/2017.
 */
public class Server {

    private File baseDir;
    private File jar;
    private Process process;
    private int xmxM;
    private int xmsM;

    public Server(File baseDir) throws NoJarFoundException {
        this.baseDir = baseDir;
        File jar;
        if((jar = Utils.getFirstJarInDir(baseDir)) == null){
            throw new NoJarFoundException(baseDir);
        }
        this.jar = jar;
    }

    public Server(String baseDir) throws NoJarFoundException {
        this(new File(baseDir));
    }

    public boolean isAlive(){
        return (process != null) && process.isAlive();
    }

    public void startServer(int xmxM, int xmsM) throws IOException{
        ProcessBuilder pb = constructProcess(xmxM, xmsM);
        process = pb.start();
    }

    public ProcessBuilder constructProcess(int xmxM, int xmsX) throws IOException {
        ProcessBuilder pb = new ProcessBuilder("java", "-jar", "-Djline.terminal=jline.UnsupportedTerminal", "-Xmx" + xmxM + "M", "-Xms" + xmsX + "M", jar.getName());
        File log = new File(baseDir + File.separator + baseDir.getName() + ".log");
        if(!log.exists()){
            log.createNewFile();
        }
        pb.redirectErrorStream(true);
        pb.redirectOutput(log);
        pb.directory(baseDir);
        Log.DEBUG.log("Directory: " + pb.directory().getAbsolutePath());
        return pb;
    }

    public void stopServer() throws IOException {
        writeInConsole("stop");
        writeInConsole("end");
    }

    public void writeInConsole(String command) throws IOException{
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(process.getOutputStream(), "UTF8"));
        bw.write(command);
        bw.newLine();
        bw.flush();
    }
}
