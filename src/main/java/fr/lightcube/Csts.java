package fr.lightcube;

/**
 * Created by Nitorac on 17/07/2017.
 */
public class Csts {

    public static final String SPLITTER = "µ~¤#";

    /*************************REDIS*****************************/
    public static final String REDIS_MAIN_CHANNEL = "lc";
    public static final String REDIS_BUNGEE = "rdsbun";
    public static final String REDIS_SPIGOT = "rdsspi";
    public static final String REDIS_ALL = "rdsall";
    public static final String REDIS_CONTROL = "rdscon";
    public static final String REDIS_SPLITTER = "@L@MC@C@";

}
