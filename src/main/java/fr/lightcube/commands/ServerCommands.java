package fr.lightcube.commands;

import com.budhash.cliche.Command;
import com.budhash.cliche.Param;
import com.google.common.base.Joiner;
import fr.lightcube.Log;
import fr.lightcube.Server;
import fr.lightcube.ServerController;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Nitorac on 18/07/2017.
 */
public class ServerCommands {

    @Command(name = "list_online_servers", abbrev = "list_on_servs")
    public void listOnlineServers(){
        List<String> aliveServers = ServerController.servers.entrySet().stream().filter(entry -> entry.getValue().isAlive()).map(Map.Entry::getKey).collect(Collectors.toList());
        Log.out("Il y a " + aliveServers.size() + " serveurs online : ");
        Log.out();
        aliveServers.forEach(name -> Log.out(" - " + name));
    }

    @Command(name = "list_offline_servers", abbrev = "list_off_servs")
    public void listOfflineServers(){
        List<String> aliveServers = ServerController.servers.entrySet().stream().filter(entry -> !entry.getValue().isAlive()).map(Map.Entry::getKey).collect(Collectors.toList());
        Log.out("Il y a " + aliveServers.size() + " serveurs offline : ");
        Log.out();
        aliveServers.forEach(name -> Log.out(" - " + name));
    }

    @Command(name = "list_all_servers", abbrev = "list")
    public void listAllServers(){
        List<String> aliveServers = ServerController.servers.entrySet().stream().map(Map.Entry::getKey).collect(Collectors.toList());
        Log.out("Il y a " + aliveServers.size() + " serveurs trouvés : ");
        Log.out();
        aliveServers.forEach(name -> Log.out(" - " + name));
    }

    @Command(name = "start_server", abbrev = "start")
    public void startServer(@Param(name="Serveur", description="Nom du serveur à démarrer")String serverName){
        Server serv = ServerController.servers.get(serverName.toLowerCase());
        if(serv == null){
            Log.out("Le serveur spécifié n'existe pas");
            return;
        }

        if(serv.isAlive()){
            Log.out("Le serveur est déjà lancé !");
            return;
        }

        Log.out("Le serveur " + serverName + " démarre ...");
        try {
            serv.startServer(1024, 512);
            Log.out("Le serveur " + serverName + " est démarré !");
        } catch (IOException e) {
            e.printStackTrace();
            Log.ERROR.log("Impossible de lancer le serveur " + serverName);
            e.printStackTrace(new PrintWriter(ServerController.bufferedWriter));
        }
    }

    @Command(name = "stop_server", abbrev = "stop")
    public void stopServer(@Param(name="Serveur", description="Nom du serveur à stopper")String serverName){
        Server serv = ServerController.servers.get(serverName.toLowerCase());
        if(serv == null){
            Log.out("Le serveur spécifié n'existe pas");
            return;
        }

        if(!serv.isAlive()){
            Log.out("Le serveur est déjà éteint !");
            return;
        }

        Log.out("Le serveur " + serverName + " s'arrête ...");
        try {
            serv.stopServer();
            boolean breaker = false;
            Log.out("Le serveur " + serverName + " est arrêté !");
        } catch (IOException e) {
            e.printStackTrace();
            Log.ERROR.log("Impossible de lancer le serveur " + serverName);
            e.printStackTrace(new PrintWriter(ServerController.bufferedWriter));
        }
    }

    @Command(name = "write", abbrev = "console")
    public void writeInConsole(@Param(name="Serveur", description="Nom du serveur")String serverName, @Param(name="Commande", description="Commande à exécuter")String... command){
        Server serv = ServerController.servers.get(serverName);
        if(serv == null){
            Log.out("Le serveur spécifié n'existe pas.");
            return;
        }
        try {
            serv.writeInConsole(Joiner.on(" ").join(command));
        } catch (IOException e) {
            e.printStackTrace();
            e.printStackTrace(new PrintWriter(ServerController.bufferedWriter));
            Log.ERROR.log("Impossible d'écrire dans la console : " + e.getMessage());
        }
    }
}
