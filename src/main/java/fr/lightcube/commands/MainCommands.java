package fr.lightcube.commands;

import com.budhash.cliche.Command;
import fr.lightcube.Log;
import fr.lightcube.ServerController;
import java.io.IOException;

/**
 * Created by Nitorac on 17/07/2017.
 */
public class MainCommands{

    @Command(name = "logs", description = "Affiche tous les logs")
    public void logs(){
        Log.getViewableLogs().forEach(System.out::println);
    }

    @Command(name = "test", description = "Test d'écriture de log")
    public void test(){
        Log.ERROR.log("TEST Log");
    }

    @Command(name = "exit", description = "Permet la fermeture du contrôleur")
    public void exit() throws IOException {
        ServerController.bufferedWriter.close();
        System.exit(0);
    }
}
