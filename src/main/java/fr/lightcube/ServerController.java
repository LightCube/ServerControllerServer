package fr.lightcube;

import com.budhash.cliche.Shell;
import com.budhash.cliche.ShellFactory;
import fr.lightcube.commands.MainCommands;
import fr.lightcube.commands.ServerCommands;
import fr.lightcube.configuration.LCConfig;
import fr.lightcube.exception.NoJarFoundException;
import fr.lightcube.nio.ServerHandler;
import fr.lightcube.redis.RedisManager;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.SelfSignedCertificate;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Nitorac on 17/07/2017.
 */
public class ServerController {

    public static final int PORT = 35698;

    private static RedisManager redisManager;

    public static Map<String, Server> servers;

    public static LCConfig generalConfig;
    public static LCConfig bungeeConfig;
    public static Shell root;

    public static BufferedWriter bufferedWriter;

    public static Channel channel;

    public static void main(String[] args) throws Exception {
        servers = new HashMap<>();
        Log.INFO.log("Dossier actuel : " + Utils.currentDir.getAbsolutePath(), true);
        Log.INFO.log("Création et écriture du fichier de logs", true);

        try {
            File logDir = new File(Utils.currentDir + File.separator + "logs");
            if(!logDir.exists()){
                logDir.mkdirs();
            }

            File logFile = new File(logDir + File.separator + new SimpleDateFormat("yyyyMMdd_HH-mm-ss").format(Calendar.getInstance().getTime()) + ".log");
            bufferedWriter = new BufferedWriter(new FileWriter(logFile));
            System.setErr(new PrintStream(logFile));
        } catch(Exception e) {
            e.printStackTrace();
            return;
        }
        Log.INFO.log("", true, true);
        Log.INFO.log("", true, true);
        Log.INFO.log("Lecture des fichiers de configuration ...", true, true);
        Log.INFO.log("", true, true);
        readConfigFiles();
        Log.INFO.log("", true, true);
        Log.INFO.log("Fichiers de configuration chargés !", true, true);
        Log.INFO.log("", true, true);
        Log.INFO.log("", true, true);
        Log.INFO.log("Initialisation du Redis ...", true, true);
        Log.INFO.log("", true, true);
        redisManager = new RedisManager();
        redisManager.init();
        Log.INFO.log("Redis chargé !", true, true);
        Log.INFO.log("Récupération de la liste des serveurs ...", true, true);
        Log.INFO.log("", true, true);
        parseServers();
        Log.INFO.log("", true, true);
        Log.INFO.log("Serveurs analysés (" + servers.size() + " trouvés) ...", true, true);
        Log.INFO.log("", true, true);
        Log.INFO.log("Initialisation de la connexion serveur ...", true, true);

        setupNio();

        Log.INFO.log("", true, true);
        Log.INFO.log("Connexion du serveur établie !", true, true);
        Log.INFO.log("", true, true);
        Log.INFO.log("Console initialisée !", true, true);

        root = ShellFactory.createConsoleShell("Server Controller", "\n------ NettyServer Controller v0.1 ------\nInterfaceur en ligne de commande\nPour afficher la liste des commandes, tapez help\n------------------------------------\n", new MainCommands(), new ServerCommands());
        root.commandLoop();
    }

    public static void readConfigFiles() throws Exception{
        try{
            generalConfig = new LCConfig(Utils.currentDir + File.separator + "general.yml");
            bungeeConfig = new LCConfig(Utils.currentDir + File.separator + "bungee" + File.separator + "config.yml");
        }catch (FileNotFoundException e){
            Log.ERROR.log(e.getMessage());
        }
    }

    public static void parseServers() throws Exception{
        File[] dirs = Utils.currentDir.listFiles(File::isDirectory);
        if(dirs == null){
            throw new FileNotFoundException("Impossible de trouver un dossier de serveur.");
        }

        Arrays.stream(dirs).forEach(d -> {
            try {
                servers.put(d.getName(), new Server(d));
            } catch (NoJarFoundException e) {
                e.printStackTrace(new PrintWriter(bufferedWriter));
            }
        });
    }

    public static void setupNio() throws Exception{
        final SslContext sslCtx;
        SelfSignedCertificate ssc = new SelfSignedCertificate();
        sslCtx = SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey()).build();

        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .handler(new LoggingHandler(LogLevel.INFO))
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    public void initChannel(SocketChannel ch) throws Exception {
                        ChannelPipeline p = ch.pipeline();
                        p.addLast(sslCtx.newHandler(ch.alloc()));
                        p.addLast(
                            new ObjectEncoder(),
                            new ObjectDecoder(ClassResolvers.cacheDisabled(null)),
                            new ServerHandler());
                    }
                });

            // Bind and start to accept incoming connections.
            channel = b.bind(PORT).awaitUninterruptibly().channel();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static RedisManager getRedisManager(){
        return redisManager;
    }
}
