package fr.lightcube.redis;

import fr.lightcube.Csts;
import fr.lightcube.Log;
import fr.lightcube.ServerController;
import redis.clients.jedis.JedisPubSub;

/**
 * Created by Nitorac on 15/04/2017.
 */
public class LCSuscriber extends JedisPubSub {
    @Override
    public void onMessage(String channel, final String msg) {
        // Doit être exécuté dans le process du serveur !
        String[] channelTitleAndMessage = msg.split(Csts.REDIS_SPLITTER, 3);
        if(!channel.equals(Csts.REDIS_MAIN_CHANNEL) || channelTitleAndMessage.length != 3) {
            Log.WARN.log("Channel Redis malformé (" + channel + " : " + msg + ")");
            return;
        }

        if(channelTitleAndMessage[0].equals(Csts.REDIS_CONTROL)){
            ServerController.getRedisManager().callAll(channelTitleAndMessage[1], channelTitleAndMessage[2]);
        }
    }

    @Override
    public void onPMessage(String s, String s2, String s3) {

    }

    @Override
    public void onSubscribe(String s, int i) {

    }

    @Override
    public void onUnsubscribe(String s, int i) {

    }

    @Override
    public void onPUnsubscribe(String s, int i) {

    }

    @Override
    public void onPSubscribe(String s, int i) {

    }
}
