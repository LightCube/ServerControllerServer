package fr.lightcube.exception;

import java.io.File;

/**
 * Created by Nitorac on 17/07/2017.
 */
public class NoJarFoundException extends Exception {

    private File baseDir;

    public NoJarFoundException(File baseDir){
        this.baseDir = baseDir;
    }

    @Override
    public String getMessage(){
        return "Aucun jar ne se trouve dans le dossier : " + baseDir.getAbsolutePath();
    }
}
