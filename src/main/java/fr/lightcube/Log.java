package fr.lightcube;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Nitorac on 17/07/2017.
 */
public enum Log {
    DEBUG("[DEBUG] : ", Utils.ANSI_CYAN),
    INFO("[INFO] : ", Utils.ANSI_GREEN),
    WARN("[WARN] : ", Utils.ANSI_YELLOW),
    ERROR("[ERROR] : ", Utils.ANSI_RED);

    static ArrayList<String> viewableLines = new ArrayList<>();

    String prefix;
    String color;

    Log(String prefix, String color){
        this.prefix = prefix;
        this.color = color;
    }

    public static void out(String... lines){
        if(lines.length == 0){
            out("");
            return;
        }
        Arrays.stream(lines).forEach(System.out::println);
    }

    public void log(String line){
        log(line, false, true);
    }

    public void log(String line, boolean display){
        log(line, display, false);
    }

    public void log(String line, boolean display, boolean writeInFile){
        String date = "[" + Utils.getNowFormatted("HH:mm:ss") + "] ";
        String logLine = Utils.ANSI_RESET + date + color + prefix + line + Utils.ANSI_RESET;
        viewableLines.add(logLine);
        if(display){
            System.out.println(logLine);
        }
        if(writeInFile){
            try {
                ServerController.bufferedWriter.newLine();
                ServerController.bufferedWriter.write(date + prefix + line);
                ServerController.bufferedWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static ArrayList<String> getViewableLogs(){
        return viewableLines;
    }
}
